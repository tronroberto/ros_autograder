#!/usr/bin/env python
""" Run rostest, format output as JSON for Gradescope """
from __future__ import print_function

import json
import os
import re
import subprocess
import sys
import xmltodict


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))

if 'ROS_TEST_RESULTS_DIR' in os.environ:
    RESULTS_DIR = os.path.join(os.environ['ROS_TEST_RESULTS_DIR'],
                               'ros_autograder')
else:
    RESULTS_DIR = os.path.join(SCRIPT_DIR, 'test_results', 'ros_autograder')

if 'ROS_AUTOGRADER_TESTS_DIR' in os.environ:
    TESTS_DIR = os.environ['ROS_AUTOGRADER_TESTS_DIR']
else:
    TESTS_DIR = os.path.join(SCRIPT_DIR, '..', 'tests')


def normalize_xml_text(s):
    s = s.replace('<![CDATA[', '')
    s = s.replace(']]>', '')
    s = re.sub(r'&#x001B;\[\d+m', '', s)
    return s


def load_xml_results(filename):
    d = {}
    fullfile = os.path.join(RESULTS_DIR, filename)
    with open(fullfile, 'r') as f_in:
        s = f_in.read()
        d = xmltodict.parse(s)
        d = d['testsuite']

    return d


def xml2json_suite(d_suite, output_header=None):
    output = normalize_xml_text(d_suite['system-err'])
    if output_header:
        output = '-- ' + d_suite['@name'] + ' ' + '-' * 10 + '\n' + output

    d_out = {
        'execution_time':
        float(d_suite['@time']),
        'output':
        output,
        'stdout_visiblity':
        'visible',
        'score':
        int(d_suite['@tests']) - int(d_suite['@failures']) -
        int(d_suite['@errors']),
        'tests':
        xml2json_units(d_suite, output_header)
    }
    return d_out


def default_description(xml_filename):
    descriptions = {
        'hz': 'Check publishing rate',
        'publish': 'Check that a topic is published at least once'
    }
    for testname, description in descriptions.items():
        if xml_filename == 'rosunit-' + testname + '.xml':
            return description + '\n'
    return ''


def xml2json_units(d_suite, suitename=None):
    test_names = [re.sub('^test', '', x['@name']) for x in d_suite['testcase']]
    file_names = ['rosunit-' + x + '.xml' for x in test_names]
    tests = []
    for xml_filename in file_names:
        d_unit = load_xml_results(xml_filename)
        max_score = int(d_unit['@tests'])
        score = max_score - int(d_unit['@failures']) - int(d_unit['@errors'])
        unitname = xml_filename
        if suitename:
            unitname = suitename + '-' + unitname
        unitname = re.sub('(\.xml|rostest-|rosunit-)', '', unitname)
        #print(unitname)
        d_unit_out = {
            'name': unitname,
            'score': score,
            'max_score': max_score,
            'output': ''
        }

        # add description to output
        description = default_description(xml_filename)
        if description:
            d_unit_out['output'] += 'Description: ' + description

        # add result status to output
        d_unit_out['output'] += 'Result: '
        if score == max_score:
            d_unit_out['output'] += 'Passed\n'
        else:
            d_unit_out['output'] += 'Failed\n'
            d_unit_out['output'] += normalize_xml_text(
                d_unit['system-out'] + d_unit['system-err']) + '\n'
            if 'failure' in d_unit['testcase']:
                d_unit_out['output'] += d_unit['testcase']['failure'][
                    '@type'] + ': ' + d_unit['testcase']['failure']['#text']

        # we are done with this test unit
        tests.append(d_unit_out)
    return tests


def xml2json(filename):
    d_suite = load_xml_results(filename)
    d_out = xml2json_suite(d_suite, filename)
    return d_out


def json_merge(d_all, d_out):
    for field in ['execution_time', 'output', 'score', 'tests']:
        d_all[field] += d_out[field]
    return d_all


def runner():
    # Find .test files
    test_list = [x for x in os.listdir(TESTS_DIR) if '.test' in x]
    d_all = None
    print(os.environ)
    for testname in test_list:
        # TODO: do not hardcode script's path
        cmd = [
            '/autograder/ws/src/ros_autograder/scripts/rostest_with_source.sh',
            testname
        ]  # ,'>',testname+'.out']
        # TODO: figure out how to avoid a subprocess call by importing rostest
        with open(testname + '.out', 'w') as outfile:
            subprocess.call(cmd, shell=True)
            # subprocess.call(cmd, stdout=outfile, shell=True)
        #    p = subprocess.Popen(cmd, stdout=outfile)
        #    os.waitpid(p.pid, 0)
        d_out = xml2json('rostest-tests_' + testname.replace('.test', '.xml'))
        if not d_all:
            d_all = d_out
        else:
            d_all = json_merge(d_all, d_out)
    print(json.dumps(d_all))
    return d_all


if __name__ == '__main__':
    runner()
