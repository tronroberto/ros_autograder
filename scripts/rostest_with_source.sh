#!/usr/bin/bash
# According to bash(1), /etc/bash.bashrc should be sourced automatically for non-login shell, but probably Gradescope uses sh instead of bash, so we need to source manually.
source /etc/bash.bashrc
rostest ros_autograder $1
