from argparse import parser
import unittest

from gradescope_utils.autograder_utils.json_test_runner import JSONTestRunner

if __name__ == '__main__':
    parser = OptionParser(usage)
    parser.add_option("-l", "--local", dest="flag_local",
                      help="Run tests using unittest (locally, without uploading on Gradescope).",
                      action="store_true", default=False)

    #Parse and check options and arguments
    (options, args) = parser.parse_args()

    suite = unittest.defaultTestLoader.discover('./tests')
    if options.flag_local:
        unittest.TextTestRunner(verbosity=1).run(suite)
    else:
        JSONTestRunner(visibility='visible').run(suite)
