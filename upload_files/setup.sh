#!/usr/bin/env bash
set -x #echo every command

cd /autograder/source
source setup_variables
cd /autograder/source
source setup_ssh
cd /autograder/source
source setup_git
cd /autograder/source
source setup_ros
cd /autograder/source
source setup_workspace
cd /autograder/source
source setup_tests

# Install pip + requirements
apt-get install -y python python-pip python-dev

pip install -r /autograder/source/requirements.txt
